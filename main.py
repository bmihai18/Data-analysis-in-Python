import pandas as pd
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB

import functii
import grafice

table = pd.read_csv("wine.csv")

var = list(table)
var_pred = var[:-1]
var_target = var[len(var) - 1]

print("############################### VAR PREDICTOR ######################################")

print(var_pred, var_target, sep="\n")
instances = list(table.index)

x = table[var_pred].values
y = table[var_target].values

# Construire model liniar - LDA
model_lda = LinearDiscriminantAnalysis()
model_lda.fit(x, y)

# Analiza model
print("############################### VECTOR CLASE ######################################")
classes = model_lda.classes_
print(classes)
# Calcul scoruri discriminante (variabile discriminante)
z = model_lda.transform(x)  # x@u
# Calcul scoruri centrii
g = model_lda.means_
zg = model_lda.transform(g)  # g@u

# q = numar axe discriminante
q = z.shape[1]
etichete_z = ["z" + str(i + 1) for i in range(q)]
if q > 1:
    grafice.plot_instante(z, zg, y, classes)
for i in range(q):
    grafice.distrib(z, i, y, classes)
grafice.show()

# Predictie in setul de antrenare
predict_lda = model_lda.predict(x)
eval_lda = functii.evaluare_model(predict_lda, y, var_target, "LDA", instances, classes)
eval_lda[0].to_csv("Clasif_baza_lda.csv")
eval_lda[1].to_csv("Err_baza_lda.csv")
eval_lda[2].to_csv("mat_conf_lda.csv")
print("############################### TABEL EVALUARE LDA ######################################")
print(eval_lda[2])

print("############################### LDA - Acc si Cohen ######################################")

print("Acuratete LDA:", eval_lda[3])
print("Index Cohen LDA:", eval_lda[4])

# Predictie in setul de aplicare-testare
tabel_test = pd.read_csv("wine_test.csv")

x_testare = tabel_test[var_pred].values

predict_lda_testare = model_lda.predict(x_testare)
tabel_test["Predictie LDA"] = predict_lda_testare

# Construire si aplicare model Bayesian
model_bayes = GaussianNB()
model_bayes.fit(x, y)

predict_bayes = model_bayes.predict(x)
eval_bayes = functii.evaluare_model(predict_bayes, y, var_target, "Bayes", instances, classes)
eval_bayes[0].to_csv("Clasif_baza_bayes.csv")
eval_bayes[1].to_csv("Err_baza_bayes.csv")
eval_bayes[2].to_csv("mat_conf_bayes.csv")
print("############################### Bayes - Evaluare, Acc, Cohen ######################################")
print(eval_bayes[2])
print("Acuratete Bayes:", eval_bayes[3])
print("Index Cohen Bayes:", eval_bayes[4])

predict_bayes_testare = model_bayes.predict(x_testare)
tabel_test["Predictie Bayes"] = predict_bayes_testare

tabel_test.to_csv("Predictie.csv")
