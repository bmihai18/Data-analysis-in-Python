import matplotlib.pyplot as plt
import numpy as np
import seaborn as sb
from matplotlib import cm


def plot_instante(z, zg, y, clase, k1=0, k2=1):
    f = plt.figure(figsize=(13, 8))
    ax = f.add_subplot(1, 1, 1)
    assert isinstance(ax, plt.Axes)
    ax.set_title("Plot instante in axele discriminante", fontdict={"fontsize": 20, "color": 'r'})
    ax.set_xlabel("u" + str(k1 + 1))
    ax.set_ylabel("u" + str(k2 + 1))
    array_clase = np.array(clase)
    q = len(clase)
    my_map = cm.get_cmap("cool", q)
    cmap = [my_map(i) for i in range(q)]
    # Parcurgere in paralel a listei de culori si a claselor
    for i in zip(cmap, clase):
        ax.scatter(z[y == i[1], k1], z[y == i[1], k2], label=i[1], color=i[0])
        ax.scatter(zg[array_clase == i[1], k1], zg[array_clase == i[1], k2], color=i[0], marker="s", s=200)
    ax.legend()


def distrib(z, k, y, clase):
    f = plt.figure(figsize=(13, 8))
    ax = f.add_subplot(1, 1, 1)
    assert isinstance(ax, plt.Axes)
    ax.set_title("Distributie in axa u" + str(k + 1), fontdict={"fontsize": 18, "color": 'r'})
    for clasa in clase:
        sb.kdeplot(z[y == clasa, k], shade=True, label=clasa, ax=ax)
    ax.legend()


def show():
    plt.show()
