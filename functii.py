import pandas as pd
import sklearn.metrics as metrics
import numpy as np


def evaluare_model(predictie, y, variabila_tinta, eticheta_model, instante, clase):
    tabel_clasificare = pd.DataFrame(
        data={
            variabila_tinta: y,
            "Predictie" + eticheta_model: predictie
        }, index=instante
    )
    tabel_err = tabel_clasificare[y != predictie]
    mat_conf = metrics.confusion_matrix(y, predictie)
    t_mat_conf = pd.DataFrame(mat_conf, clase, clase)
    n = len(y)
    n_err = len(tabel_err)
    acuratete = np.round(100 - n_err * 100 / n, 4)
    t_mat_conf["Acuratete"] = np.round(np.diagonal(mat_conf) * 100 / np.sum(mat_conf, axis=1), 2)
    index_cohen = np.round(metrics.cohen_kappa_score(y, predictie),4)
    return tabel_clasificare, tabel_err, t_mat_conf, acuratete, index_cohen
